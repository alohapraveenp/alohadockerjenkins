FROM ubuntu:16.10

RUN apt-get update && apt-get -y install software-properties-common python-software-properties  php7.0 php7.0-mysql git curl wget sudo unzip vim && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN /usr/sbin/a2dismod 'mpm_*' && /usr/sbin/a2enmod mpm_prefork
RUN add-apt-repository "deb http://cz.archive.ubuntu.com/ubuntu yakkety web universe"
#RUN apt-get install -y sysv-rc-conf
RUN export TERM=xterm

RUN useradd -p $(openssl passwd -1 aloha) aloha -m -s /bin/bash
RUN usermod aloha -G root,www-data

#Jenkins
RUN wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
RUN sh -c 'echo deb http://pkg.jenkins.io/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
RUN apt-get update
RUN apt-get install -y jenkins
RUN update-rc.d jenkins defaults
ADD user-config.xml /var/lib/jenkins/users/root/config.xml
ADD jenkins-job.xml /var/lib/jenkins/jobs/DockerImage/config.xml
ADD jenkins-plugins.txt /var/lib/jenkins/jenkins-plugins.txt
RUN mkdir -p /var/jenkins/sem
RUN chown -R jenkins:jenkins /var/jenkins
RUN chown -R jenkins:jenkins /var/lib/jenkins
RUN chmod 777 /var/jenkins/sem
ADD .aws/* /var/lib/jenkins/.aws/
ADD .aws/* /.aws/
RUN chown -R jenkins:jenkins /var/lib/jenkins
RUN chmod 600 /var/lib/jenkins/.aws/

#Maven
#COPY src/ /var/www/html/
#RUN apt-get install -y maven
#RUN apt-get install -y openjdk-8-jdk

# Install AWS CLI
#RUN apt-get install -y  python-pip
#RUN pip install --upgrade pip
#RUN pip install --upgrade --user awscli
RUN curl -sS https://s3.amazonaws.com/aws-cli/awscli-bundle.zip -o awscli-bundle.zip
RUN unzip awscli-bundle.zip
RUN python3 awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
RUN rm -rf awscli-bundle awscli-bundle.zip

# Install Docker
RUN apt-get -y install docker.io
RUN ln -sf /usr/bin/docker.io /usr/local/bin/docker
RUN usermod -aG docker jenkins

#RUN service jenkins start
#RUN service docker start
ADD jenkins.sh /usr/local/bin/jenkins.sh
RUN chmod +x /usr/local/bin/jenkins.sh
ADD run.sh /usr/local/bin/run.sh
RUN chmod +x /usr/local/bin/run.sh


EXPOSE 80 443 8080

#CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
CMD ["/usr/local/bin/run.sh"]
