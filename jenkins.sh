#!/bin/bash
file="/var/lib/jenkins/jenkins-plugins.txt"
jarfile="/var/lib/jenkins/jenkins-cli.jar"
while [ ! -s $jarfile ]
do
        wget -q http://localhost:8080/jnlpJars/jenkins-cli.jar -O $jarfile
        chown jenkins:jenkins $jarfile
done
while IFS= read plugin
do
        java -jar $jarfile -s http://localhost:8080 install-plugin $plugin >> /var/log/jenkins/jenkins.log 2>&1
done <"$file"
service jenkins restart
